<?php

namespace MarsRover\Services;

use MarsRover\Exceptions\FileException;

class FileReader
{
    private string $content;

    public function __construct(string $file)
    {
        if (!file_exists($file))
            throw (new FileException())->fileNotExistForParse();

        $this->content = file_get_contents($file);
    }

    public function getContent(): string
    {
        return $this->content;
    }
}