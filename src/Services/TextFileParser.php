<?php

namespace MarsRover\Services;

use MarsRover\Exceptions\FileException;
use MarsRover\Exceptions\ParsingException;
use MarsRover\Models\Coordinate;
use MarsRover\Models\Directions;
use MarsRover\Models\Moves;
use MarsRover\Models\Plateau;
use MarsRover\Models\Rover;
use MarsRover\Models\RoverCoordinate;
use MarsRover\Models\RoverSquad;

class TextFileParser extends Parser
{
    private array $lines;

    public function __construct(string $content)
    {
        $this->lines = explode(PHP_EOL, $content);
    }

    private function parsePlateauString(string $command): void
    {
        $parseCoordinates = explode(' ', $command);
        if (count($parseCoordinates) === 2 and is_numeric($parseCoordinates[0]) and is_numeric($parseCoordinates[1])) {
            $coordinate = new Coordinate($parseCoordinates[0], $parseCoordinates[1]);
            $this->plateau = new Plateau($coordinate);
        } else {
            throw (new ParsingException())->incorrectInitialPlateau();
        }
    }

    private function roverCoordinateGenerator(string $command): RoverCoordinate
    {
        $parseCoordinates = explode(' ', $command);
        if (count($parseCoordinates) === 3
            && is_numeric($parseCoordinates[0])
            && is_numeric($parseCoordinates[1])
            && Directions::validate($parseCoordinates[2]))
            return new RoverCoordinate($parseCoordinates[0], $parseCoordinates[1], $parseCoordinates[2]);
        throw (new ParsingException())->incorrectInitialRover();
    }

    private function roverMovesGenerator(string $command): Moves
    {
        $parseCoordinates = str_split($command);
        $moves = new Moves();
        foreach ($parseCoordinates as $move) {
            $moves->addMove($move);
        }
        return $moves;
    }

    public function parse(): void
    {
        $this->parsePlateauString($this->lines[0]);
        unset($this->lines[0]);

        $roverSquad = new RoverSquad();
        foreach ($this->lines as $key => $value) {
            if ($key % 2 !== 0) {
                $rover = new Rover($this->roverCoordinateGenerator($value));
                $rover->setMoves($this->roverMovesGenerator($this->lines[$key+1]));
                $roverSquad->appendRover($rover);
            }
        }

        $this->roverSquad = $roverSquad;
    }
}