<?php

namespace MarsRover\Services;

use MarsRover\Interfaces\PlateauInterface;
use MarsRover\Models\RoverSquad;

abstract class Parser
{
    protected PlateauInterface $plateau;
    protected RoverSquad $roverSquad;

    public function getPlateau(): PlateauInterface
    {
        return $this->plateau;
    }

    public function getRoverSquad(): RoverSquad
    {
        return $this->roverSquad;
    }

    abstract public function parse(): void;
}