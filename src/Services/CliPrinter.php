<?php

namespace MarsRover\Services;

use MarsRover\Interfaces\PrinterInterface;
use MarsRover\Interfaces\RoverInterface;

class CliPrinter implements PrinterInterface
{
    public function printRoverPosition(RoverInterface $rover): void
    {
        echo $rover->currentPosition() . PHP_EOL;
    }
}