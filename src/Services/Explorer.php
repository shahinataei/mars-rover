<?php

namespace MarsRover\Services;

use MarsRover\Interfaces\PlateauInterface;
use MarsRover\Interfaces\PrinterInterface;
use MarsRover\Interfaces\RoverInterface;

class Explorer
{
    public function __construct(private PlateauInterface $plateau, private RoverInterface $rover, private PrinterInterface $printer)
    {

    }

    public function explore()
    {
        $this->rover->run($this->plateau);
        $this->printer->printRoverPosition($this->rover);
    }
}