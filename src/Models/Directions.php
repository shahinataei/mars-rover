<?php

namespace MarsRover\Models;

use MarsRover\Exceptions\MovesException;

final class Directions
{
    const NORTH = 'N';
    const WEST = 'W';
    const EAST = 'E';
    const SOUTH = 'S';

    public static function validate(string $direction): bool
    {
        if (str_contains($direction, self::NORTH)
            or str_contains($direction, self::WEST)
            or str_contains($direction, self::EAST)
            or str_contains($direction, self::SOUTH))
            return true;
        return false;
    }
}