<?php

namespace MarsRover\Models;

use MarsRover\Exceptions\DirectionException;

class RoverCoordinate extends Coordinate
{
    private string $face;

    public function __construct(int $x, int $y, string $face)
    {
        parent::__construct($x, $y);
        if(!Directions::validate($face))
            throw (new DirectionException())->incorrectDirectionFace();
        $this->face = $face;
    }

    public function getFace(): string
    {
        return $this->face;
    }

    public function __toString(): string
    {
        return implode(' ', [$this->x, $this->y, $this->face]);
    }
}