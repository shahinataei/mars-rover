<?php

namespace MarsRover\Models;

use MarsRover\Exceptions\PlateauException;
use MarsRover\Interfaces\MovesInterface;
use MarsRover\Interfaces\PlateauInterface;
use MarsRover\Interfaces\RoverInterface;

class Rover implements RoverInterface
{
    private int $x;
    private int $y;
    private string $face;
    private MovesInterface $moves;

    public function __construct(RoverCoordinate $coordinate)
    {
        $this->x = $coordinate->getX();
        $this->y = $coordinate->getY();
        $this->face = $coordinate->getFace();
    }

    public function currentPosition(): RoverCoordinate
    {
        return (new RoverCoordinate($this->x, $this->y, $this->face));
    }

    public function turnRight(): void
    {
        switch ($this->face) {
            case Directions::NORTH:
                $this->face = Directions::EAST;
                break;
            case Directions::EAST:
                $this->face = Directions::SOUTH;
                break;
            case Directions::SOUTH:
                $this->face = Directions::WEST;
                break;
            case Directions::WEST:
                $this->face = Directions::NORTH;
                break;
        }
    }

    public function turnLeft(): void
    {
        switch ($this->face) {
            case Directions::NORTH:
                $this->face = Directions::WEST;
                break;
            case Directions::WEST:
                $this->face = Directions::SOUTH;
                break;
            case Directions::SOUTH:
                $this->face = Directions::EAST;
                break;
            case Directions::EAST:
                $this->face = Directions::NORTH;
                break;
        }
    }

    public function move(PlateauInterface $plateau): void
    {
        switch ($this->face) {
            case "N":
                if(!$plateau->isInArea(new Coordinate($this->x, $this->y + 1)))
                    throw (new PlateauException())->RoverOutOfArea();
                $this->y++;
                break;
            case "W":
                if(!$plateau->isInArea(new Coordinate($this->x - 1, $this->y)))
                    throw (new PlateauException())->RoverOutOfArea();
                $this->x--;
                break;
            case "E":
                if(!$plateau->isInArea(new Coordinate($this->x + 1, $this->y)))
                    throw (new PlateauException())->RoverOutOfArea();
                $this->x++;
                break;
            case "S":
                if(!$plateau->isInArea(new Coordinate($this->x, $this->y-1)))
                    throw (new PlateauException())->RoverOutOfArea();
                $this->y--;
                break;
        }
    }

    public function setMoves(MovesInterface $moves): void
    {
        $this->moves = $moves;
    }

    public function run(PlateauInterface $plateau): void
    {
        foreach ($this->moves->getMoves() as $move) {
            switch ($move) {
                case Moves::MOVE:
                    $this->move($plateau);
                    break;
                case Moves::RIGHT:
                    $this->turnRight();
                    break;
                case Moves::LEFT:
                    $this->turnLeft();
                    break;
            }
        }
    }
}