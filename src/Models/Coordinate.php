<?php

namespace MarsRover\Models;

class Coordinate
{
    public function __construct(protected int $x, protected int $y){}

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function __toString(): string
    {
        return implode(' ', [$this->x, $this->y]);
    }
}