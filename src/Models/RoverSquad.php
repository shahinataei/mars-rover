<?php

namespace MarsRover\Models;

use MarsRover\Interfaces\RoverInterface;

class RoverSquad implements \Iterator
{
    private array $squad;
    private int $position;

    public function __construct()
    {
        $this->position = 0;
    }

    public function appendRover(RoverInterface $rover): void
    {
        $this->squad[$this->position] = $rover;
        $this->position++;
    }

    public function current(): Rover
    {
        return $this->squad[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
        return isset($this->squad[$this->position]);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }
}