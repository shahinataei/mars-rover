<?php

namespace MarsRover\Models;

use MarsRover\Interfaces\PlateauInterface;

class Plateau implements PlateauInterface
{
    private int $x;
    private int $y;

    public function __construct(Coordinate $coordinate)
    {
        $this->x = $coordinate->getX();
        $this->y = $coordinate->getY();
    }

    public function isInArea(Coordinate $coordinate): bool
    {
        if($coordinate->getX() > $this->x
            or $coordinate->getY() > $this->y
            or $coordinate->getY() < 0
            or $coordinate->getX() < 0)
            return false;
        return true;
    }

    public function __toString(): string
    {
        return implode(' ', [$this->x, $this->y]);
    }
}