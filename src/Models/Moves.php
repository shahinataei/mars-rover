<?php

namespace MarsRover\Models;

use MarsRover\Exceptions\MovesException;
use MarsRover\Interfaces\MovesInterface;

class Moves implements MovesInterface
{
    const RIGHT = 'R';
    const LEFT = 'L';
    const MOVE = 'M';

    private array $moves;

    public static function validate(string $move): bool
    {
        if (str_contains($move, self::LEFT) or str_contains($move, self::RIGHT) or str_contains($move, self::MOVE))
            return true;
        return false;
    }

    public function addMove(string $move): void
    {
        if (!self::validate($move) or strlen($move) > 1)
            throw new MovesException('This command does not exist in the listed move set!');
        $this->moves[] = $move;
    }

    public function getMoves(): array
    {
        return $this->moves;
    }
}