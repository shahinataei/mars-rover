<?php

namespace MarsRover\Exceptions;

class ParsingException extends \Exception
{
    public function incorrectInitialRover()
    {
        $this->message = 'Rover initial coordinate is not correct!';
        return $this;
    }

    public function incorrectInitialPlateau()
    {
        $this->message = 'Initial plateau command on input file is not correct!';
        return $this;
    }
}