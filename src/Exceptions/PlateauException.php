<?php

namespace MarsRover\Exceptions;

class PlateauException extends \Exception
{
    public function RoverOutOfArea(): PlateauException
    {
        $this->message = 'Your Rover will leave the plateau with current commands!';
        return $this;
    }
}