<?php

namespace MarsRover\Exceptions;

class DirectionException extends \Exception
{
    public function incorrectDirectionFace()
    {
        $this->message = 'This direction face to create rover coordinate is not correct!';
        return $this;
    }
}