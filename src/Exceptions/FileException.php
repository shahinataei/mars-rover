<?php

namespace MarsRover\Exceptions;

class FileException extends \Exception
{
    public function fileNotExistForParse()
    {
        $this->message = 'Input file for parsing is not exist!';
        return $this;
    }
}