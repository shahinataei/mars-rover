<?php

namespace MarsRover\Interfaces;

interface MovesInterface
{
    public function addMove(string $move): void;
    public function getMoves(): array;
}