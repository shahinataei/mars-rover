<?php

namespace MarsRover\Interfaces;

use MarsRover\Models\RoverCoordinate;

interface RoverInterface
{
    public function currentPosition(): RoverCoordinate;
    public function turnRight(): void;
    public function turnLeft(): void;
    public function move(PlateauInterface $plateau): void;
    public function setMoves(MovesInterface $moves): void;
    public function run(PlateauInterface $plateau): void;
}