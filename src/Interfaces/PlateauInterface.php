<?php

namespace MarsRover\Interfaces;

use MarsRover\Models\Coordinate;

interface PlateauInterface
{
    public function isInArea(Coordinate $coordinate): bool;
}