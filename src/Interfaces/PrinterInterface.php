<?php

namespace MarsRover\Interfaces;

interface PrinterInterface
{
    public function printRoverPosition(RoverInterface $rover): void;
}