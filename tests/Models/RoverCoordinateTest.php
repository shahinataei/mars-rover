<?php

namespace Tests\Models;

use MarsRover\Models\RoverCoordinate;
use Tests\Infrastructure;

class RoverCoordinateTest extends Infrastructure
{
    public function testCreateRoverCoordinate()
    {
        $roverCoordinate = new RoverCoordinate(1,2, 'N');
        $this->assertEquals('1 2 N', (string)$roverCoordinate);
    }

    public function testCurrentFaceOfRoverCoordinate()
    {
        $roverCoordinate = new RoverCoordinate(1,2, 'N');
        $this->assertEquals('N', $roverCoordinate->getFace());
    }
}