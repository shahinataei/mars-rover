<?php

namespace Tests\Models;

use MarsRover\Interfaces\PlateauInterface;
use MarsRover\Models\Coordinate;
use MarsRover\Models\Plateau;
use MarsRover\Models\RoverCoordinate;
use Tests\Infrastructure;

class PlateauTest extends Infrastructure
{
    public function testCreatePlateau()
    {
        $this->assertEquals('5 5', $this->plateau);
    }

    public function testCheckIsInAreaForRoverMoves()
    {
        $coordinate = new RoverCoordinate(1, 2, 'N');
        $this->assertTrue($this->plateau->isInArea($coordinate));
        $coordinate = new RoverCoordinate(6, 2, 'N');
        $this->assertFalse($this->plateau->isInArea($coordinate));
        $coordinate = new RoverCoordinate(2, 6, 'N');
        $this->assertFalse($this->plateau->isInArea($coordinate));
    }
}