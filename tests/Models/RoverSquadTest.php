<?php

namespace Tests\Models;

use MarsRover\Models\RoverSquad;
use Tests\Infrastructure;

class RoverSquadTest extends Infrastructure
{
    public function testCreateRoverSquad()
    {
        $roverSquad = new RoverSquad();
        $roverSquad->appendRover($this->rover);
        foreach ($roverSquad as $rover) {
            $this->assertEquals($this->rover, $rover);
        }
    }
}