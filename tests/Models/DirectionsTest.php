<?php

namespace Tests\Models;

use MarsRover\Models\Directions;
use Tests\Infrastructure;

class DirectionsTest extends Infrastructure
{
    public function testValidateOfDirectionString()
    {
        $this->assertTrue(Directions::validate('N'));
        $this->assertTrue(Directions::validate('S'));
        $this->assertTrue(Directions::validate('W'));
        $this->assertTrue(Directions::validate('E'));
        $this->assertFalse(Directions::validate('D'));
        $this->assertFalse(Directions::validate('m'));
    }
}