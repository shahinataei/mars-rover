<?php

namespace Tests\Models;

use MarsRover\Exceptions\DirectionException;
use MarsRover\Exceptions\PlateauException;
use MarsRover\Models\Moves;
use MarsRover\Models\Rover;
use MarsRover\Models\RoverCoordinate;
use Tests\Infrastructure;

class RoverTest extends Infrastructure
{
    public function setUp(): void
    {
        $roverCoordinate = new RoverCoordinate(1, 2, 'N');
        $this->rover = new Rover($roverCoordinate);
    }

    public function testInitialRover()
    {
        $this->assertEquals(new RoverCoordinate(1, 2, 'N'), $this->rover->currentPosition());
    }

    public function testCreateRoverByWrongCoordinate()
    {
        $this->expectException(DirectionException::class);
        $coordinate = new RoverCoordinate(1, 3, 'M');
        new Rover($coordinate);
    }

    public function testTurnRightRover()
    {
        $this->rover->turnRight();
        $this->assertEquals(new RoverCoordinate(1, 2, 'E'), $this->rover->currentPosition());
    }

    public function testTurnLeftRover()
    {
        $this->rover->turnLeft();
        $this->assertEquals(new RoverCoordinate(1, 2, 'W'), $this->rover->currentPosition());
    }

    public function testMoveOfRover()
    {
        $this->rover->move($this->plateau);
        $this->assertEquals(new RoverCoordinate(1, 3, 'N'), $this->rover->currentPosition());
    }

    public function testMoveToOutOfRover()
    {
        $this->expectException(PlateauException::class);
        $this->rover->move($this->plateau);
        $this->rover->move($this->plateau);
        $this->rover->move($this->plateau);
        $this->rover->move($this->plateau);
        $this->rover->move($this->plateau);
    }

    public function testAddMovesToRover()
    {
        $moves = new Moves();
        $moves->addMove('M');
        $moves->addMove('R');
        $moves->addMove('M');
        $moves->addMove('M');
        $moves->addMove('R');
        $this->rover->setMoves($moves);
        $this->rover->run($this->plateau);
        $this->assertEquals(new RoverCoordinate(3, 3, 'S'), $this->rover->currentPosition());
    }
}