<?php

namespace Tests\Models;

use MarsRover\Models\Coordinate;
use Tests\Infrastructure;

class CoordinateTest extends Infrastructure
{
    public function testCreateCoordinate()
    {
        $coordinate = new Coordinate(2,3);
        $this->assertEquals('2 3', $coordinate);
    }
}