<?php

namespace Tests\Models;

use MarsRover\Models\Moves;
use Tests\Infrastructure;

class MovesTest extends Infrastructure
{
    public function testValidateOfMoves()
    {
        $this->assertTrue(Moves::validate('L'));
        $this->assertTrue(Moves::validate('R'));
        $this->assertTrue(Moves::validate('M'));
        $this->assertFalse(Moves::validate('a'));
        $this->assertFalse(Moves::validate('B'));
    }

    public function testAddMoveAndGetMoves()
    {
        $moves = new Moves();
        $moves->addMove('L');
        $this->assertEquals(['L'], $moves->getMoves());
    }
}