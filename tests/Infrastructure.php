<?php
namespace Tests;

use MarsRover\Interfaces\PlateauInterface;
use MarsRover\Interfaces\RoverInterface;
use MarsRover\Models\Coordinate;
use MarsRover\Models\Moves;
use MarsRover\Models\Plateau;
use MarsRover\Models\Rover;
use MarsRover\Models\RoverCoordinate;
use MarsRover\Models\RoverSquad;
use MarsRover\Services\FileReader;
use Mockery;
use PHPUnit\Framework\TestCase;

class Infrastructure extends TestCase
{
    protected PlateauInterface $plateau;
    protected RoverInterface $rover;
    protected FileReader $fileReader;
    protected RoverSquad $roverSquad;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $coordinate = new Coordinate(5,5);
        $this->plateau = new Plateau($coordinate);

        $roverCoordinate = new RoverCoordinate(1, 2, 'W');
        $moves = new Moves();
        $moves->addMove('L');
        $moves->addMove('M');
        $moves->addMove('L');
        $moves->addMove('M');
        $moves->addMove('L');
        $moves->addMove('M');
        $moves->addMove('L');
        $moves->addMove('M');
        $moves->addMove('M');
        $this->rover = new Rover($roverCoordinate);
        $this->rover->setMoves($moves);

        $this->fileReader = Mockery::mock(FileReader::class);
        $this->fileReader->shouldReceive([
            'getContent' => '5 5'.PHP_EOL.'1 2 W'.PHP_EOL.'LMLMLMLMM'
        ]);

        $this->roverSquad = new RoverSquad();
        $this->roverSquad->appendRover($this->rover);
    }
}