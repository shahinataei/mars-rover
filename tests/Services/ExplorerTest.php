<?php

namespace Tests\Services;

use MarsRover\Services\CliPrinter;
use MarsRover\Services\Explorer;
use Tests\Infrastructure;

class ExplorerTest extends Infrastructure
{
    private Explorer $explorer;

    public function testResultOfExploreWithCliPrinting()
    {
        $expected = '0 2 W'.PHP_EOL;
        $this->expectOutputString($expected);
        $this->explorer = new Explorer($this->plateau, $this->rover, new CliPrinter());
        $this->explorer->explore();
    }
}