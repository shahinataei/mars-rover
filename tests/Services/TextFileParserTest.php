<?php

namespace Tests\Services;

use MarsRover\Models\Plateau;
use MarsRover\Services\Parser;
use MarsRover\Services\TextFileParser;
use Tests\Infrastructure;

class TextFileParserTest extends Infrastructure
{
    private Parser $textFileParser;

    public function setUp(): void
    {
        $this->textFileParser = new TextFileParser($this->fileReader->getContent());
        $this->textFileParser->parse();
    }

    public function testResultOfPlateauAfterParsing()
    {
        $this->assertEquals($this->plateau ,$this->textFileParser->getPlateau());
    }

    public function testResultOfRoverSquadAfterPArsing()
    {
        $this->assertEquals($this->roverSquad, $this->textFileParser->getRoverSquad());
    }
}