<?php

namespace Tests\Services;

use MarsRover\Services\CliPrinter;
use Tests\Infrastructure;

class CliPrinterTest extends Infrastructure
{
    public function testPrintRoverPositionResult()
    {
        $expected = '1 2 W'.PHP_EOL;
        $this->expectOutputString($expected);
        $printer = new CliPrinter();
        $printer->printRoverPosition($this->rover);
    }
}