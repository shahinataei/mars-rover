<?php

namespace Tests\Services;

use bovigo\vfs\content\LargeFileContent;
use bovigo\vfs\vfsStream;
use bovigo\vfs\vfsStreamFile;
use MarsRover\Exceptions\FileException;
use MarsRover\Services\FileReader;
use Tests\Infrastructure;

class FileReaderTest extends Infrastructure
{
    private $file;

    public function setUp(): void
    {
        $root = vfsStream::setup('root');
        $this->file = vfsStream::newFile('commands.txt')
            ->withContent('MM')
            ->at($root);
    }

    public function testResultOfNotExistingFileForFileReader()
    {
        $this->expectException(FileException::class);
        new FileReader('filenamessssswwww.txt');

    }

    public function testResultOfReadingContentFile()
    {
        $reader = new FileReader($this->file->url());
        $this->assertEquals('MM', $reader->getContent());

    }
}