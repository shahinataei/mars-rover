<?php

use MarsRover\Services\CliPrinter;
use MarsRover\Services\Explorer;
use MarsRover\Services\TextFileParser;
use MarsRover\Exceptions\FileException;
use MarsRover\Exceptions\PlateauException;
use MarsRover\Exceptions\DirectionException;
use MarsRover\Exceptions\MovesException;
use MarsRover\Exceptions\ParsingException;
use MarsRover\Services\FileReader;

require __DIR__ . '/vendor/autoload.php';

try {
    $fileReader = new FileReader('commands.txt');
    $fileParser = new TextFileParser($fileReader->getContent());
    $fileParser->parse();
    $printer = new CliPrinter();
    foreach ($fileParser->getRoverSquad() as $rover) {
        $explorer = new Explorer($fileParser->getPlateau(), $rover, $printer);
        $explorer->explore();
    }
} catch (FileException $e) {
    echo 'File Parsing: ' . $e->getMessage();
} catch (PlateauException $e) {
    echo 'Plateau: ' . $e->getMessage();
} catch (DirectionException $e) {
    echo 'Direction: ' . $e->getMessage();
} catch (MovesException $e) {
    echo 'Moves: ' . $e->getMessage();
} catch (ParsingException $e) {
    echo 'Parsing: ' . $e->getMessage();
} catch (Exception $e) {
    echo $e->getMessage();
}